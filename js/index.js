"use strict";



const tabsTitles = Array.from(document.querySelectorAll(".tabs-title"));
const tabsContentLists = Array.from(document.querySelectorAll(".tabs-content-list"));

tabsTitles.forEach((el) => {el.addEventListener("click", () => {

    tabsTitles.forEach((el) => el.classList.remove("active"));
    tabsContentLists.forEach((el) => el.classList.remove("active"));

    el.classList.add("active");
    const content = document.querySelector("#" + el.dataset.tab);
    content.classList.add("active");
  });
});


